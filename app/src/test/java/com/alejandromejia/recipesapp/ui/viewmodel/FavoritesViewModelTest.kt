package com.alejandromejia.recipesapp.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.domain.DeleteRecipeDBUseCase
import com.alejandromejia.recipesapp.domain.GetAllRecipesDBUseCase
import com.alejandromejia.recipesapp.domain.GetRecipeDBById
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class FavoritesViewModelTest {

    @RelaxedMockK
    private lateinit var getAllRecipesDBUseCase: GetAllRecipesDBUseCase

    @RelaxedMockK
    private lateinit var getRecipeDBById: GetRecipeDBById

    @RelaxedMockK
    private lateinit var deleteRecipeDBUseCase: DeleteRecipeDBUseCase

    private lateinit var favoritesViewModel: FavoritesViewModel


    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()


    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        favoritesViewModel =
            FavoritesViewModel(getAllRecipesDBUseCase, getRecipeDBById, deleteRecipeDBUseCase)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }


    @Test
    fun whenViewModelIsCreatedGetAllRecipesSavedInDataBase() = runTest {

        //Given
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myResult = listOf(Root(1, 1, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true))

        coEvery { getAllRecipesDBUseCase() } returns myResult

        //When

        favoritesViewModel.onCreate()

        //Then

        assert(favoritesViewModel.completeRecipe.value == myResult)

    }

    @Test
    fun whenSearchRecipeByIdInDataBase() = runTest {

        //Given
        val id = 1
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myRoot = Root(1, 2, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true)

        coEvery { getRecipeDBById(id) } returns myRoot

        //When

        favoritesViewModel.searchRecipeById(id)

        //Then

        assert(favoritesViewModel.root.value == myRoot)

    }


}