package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.Root
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class PutRecipeDbUseCaseTest {

    @RelaxedMockK
    private lateinit var repository: RecipeRepository

    lateinit var putRecipeDbUseCase: PutRecipeDbUseCase

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        putRecipeDbUseCase = PutRecipeDbUseCase(repository)
    }

    @Test
    fun whenDataBaseReturnsARecipeSearchedById() = runBlocking {

        //Given
        val id = 1
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myRoot = Root(1, 2, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true)
        val unit = Unit

        coEvery { repository.saveRecipe(myRoot) } returns unit

        //When

        putRecipeDbUseCase(myRoot)

        //Then

        coVerify(exactly = 1) { repository.saveRecipe(myRoot) }

    }
}