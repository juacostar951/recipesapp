package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.FoodRecipe
import com.alejandromejia.recipesapp.data.model.Recipe
import com.alejandromejia.recipesapp.data.model.Root
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class GetRecipeByIdTest{

    @RelaxedMockK
    private lateinit var repository: RecipeRepository

    lateinit var getRecipeById: GetRecipeById

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        getRecipeById = GetRecipeById(repository)
    }

    @Test
    fun whenApiReturnsARecipeSearchedById() = runBlocking {

        //Given
        val id = 1
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myRoot = Root(1, 2, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true)

        coEvery { repository.getCompleteRecipe(id) } returns myRoot

        //When

        getRecipeById(id)

        //Then

        coVerify(exactly = 1) { repository.getCompleteRecipe(id) }

    }
}