package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.FoodRecipe
import com.alejandromejia.recipesapp.data.model.Recipe
import com.alejandromejia.recipesapp.data.model.Result
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test


class GetRecipeUseCaseTest {

    @RelaxedMockK
    private lateinit var repository: RecipeRepository

    lateinit var getRecipeUseCase: GetRecipeUseCase

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        getRecipeUseCase = GetRecipeUseCase(repository)
    }

    @Test
    fun whenApiReturnsARandomRecipesServerResponse() = runBlocking {

        //Given
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myResult = listOf(Recipe(1, 1, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true))
        val myFoodRecipe = FoodRecipe(myResult)
        coEvery { repository.getAllRecipes() } returns myFoodRecipe

        //When

        getRecipeUseCase()

        //Then

        coVerify(exactly = 1) { repository.getAllRecipes() }

    }

}