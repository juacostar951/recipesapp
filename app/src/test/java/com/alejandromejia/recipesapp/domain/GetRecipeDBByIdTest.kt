package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.Root
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetRecipeDBByIdTest {

    @RelaxedMockK
    private lateinit var repository: RecipeRepository

    lateinit var getRecipeDBById: GetRecipeDBById

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        getRecipeDBById = GetRecipeDBById(repository)
    }

    @Test
    fun whenDataBaseReturnsARecipeSearchedById() = runBlocking {

        //Given
        val id = 1
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myRoot = Root(1, 2, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true)

        coEvery { repository.getRecipeDBById(id) } returns myRoot

        //When

        getRecipeDBById(id)

        //Then

        coVerify(exactly = 1) { repository.getRecipeDBById(id) }

    }

}