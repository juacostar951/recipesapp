package com.alejandromejia.recipesapp.data

import androidx.appcompat.app.AppCompatActivity
import com.alejandromejia.recipesapp.data.database.dao.RecipeDao
import com.alejandromejia.recipesapp.data.database.entities.RootEntity
import com.alejandromejia.recipesapp.data.model.FoodRecipe
import com.alejandromejia.recipesapp.data.model.FoodRecipeSimple
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.data.network.RecipeService
import com.alejandromejia.recipesapp.data.network.model.RecipeMapper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RecipeRepository @Inject constructor(private val recipeDao: RecipeDao) : AppCompatActivity(){

    private val api = RecipeService()
    private val recipeMapper = RecipeMapper()


    suspend fun getAllRecipes(): FoodRecipe {
        return api.getRecipe()
    }

    suspend fun getRecipesFilter(query: String): FoodRecipeSimple {
        return api.getRecipeByFilter(query)
    }

    suspend fun getCompleteRecipe(id: Int): Root {
        return api.getCompleteRecipe(id)

    }

    suspend fun getRecipeDBById(id: Int): Root {
        val entity = recipeDao.getById(id)
        return recipeMapper.mapFromEntity(entity)
    }

    suspend fun saveRecipe(root: Root) {
        val entity: RootEntity = recipeMapper.mapToEntity(root)
        recipeDao.insertRecipe(entity)
    }

    suspend fun getAllRecipesFromDB() : List<Root>{
        val entity = recipeDao.getAllRecipesDB()
        return recipeMapper.fromEntityList(entity)
    }

    suspend fun deleteRecipe(root: Root) : List<Root>{
        recipeDao.delete(recipeMapper.mapToEntity(root))

        return getAllRecipesFromDB()
    }

}