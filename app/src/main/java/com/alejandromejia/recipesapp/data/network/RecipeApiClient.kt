package com.alejandromejia.recipesapp.data.network

import com.alejandromejia.recipesapp.data.model.FoodRecipe
import com.alejandromejia.recipesapp.data.model.FoodRecipeSimple
import com.alejandromejia.recipesapp.data.model.Root
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface RecipeApiClient {

    @GET("/recipes/random")
    suspend fun getAllRecipes(
        @QueryMap queryKey: Map<String, String>
    ): Response<FoodRecipe>

    @GET("/recipes/complexSearch")
    suspend fun getRecipesFilter(
        @QueryMap queryKey: Map<String, String>
    ): Response<FoodRecipeSimple>

    @GET("/recipes/{id}/information")
    suspend fun getCompleteRecipe(
        @Path("id") id: Int,
        @QueryMap queryKey: Map<String, String>
    ): Response<Root>

}