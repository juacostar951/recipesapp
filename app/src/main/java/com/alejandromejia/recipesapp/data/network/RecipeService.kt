package com.alejandromejia.recipesapp.data.network

import com.alejandromejia.recipesapp.core.RetrofitHelper
import com.alejandromejia.recipesapp.data.model.FoodRecipe
import com.alejandromejia.recipesapp.data.model.FoodRecipeSimple
import com.alejandromejia.recipesapp.data.model.Root
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RecipeService {

    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getRecipe(): FoodRecipe {
        return withContext(Dispatchers.IO) {

            val queries: Map<String, String> = mapOf(
                "apiKey" to "ee8c2f1c3f524e309cf85a53a1e74b77",
                "number" to "10"
            )

            val response = retrofit.create(RecipeApiClient::class.java).getAllRecipes(queries)
            response.body()!!
        }

    }

    suspend fun getRecipeByFilter(query: String): FoodRecipeSimple {
        return withContext(Dispatchers.IO) {

            val queries: Map<String, String> = mapOf(
                "apiKey" to "ee8c2f1c3f524e309cf85a53a1e74b77",
                "number" to "10",
                "query" to query
            )

            val response = retrofit.create(RecipeApiClient::class.java).getRecipesFilter(queries)
            response.body()!!
        }

    }

    suspend fun getCompleteRecipe(id: Int): Root {
        return withContext(Dispatchers.IO) {

            val queries: Map<String, String> = mapOf(
                "apiKey" to "ee8c2f1c3f524e309cf85a53a1e74b77",
            )

            val response =
                retrofit.create(RecipeApiClient::class.java).getCompleteRecipe(id, queries)
            response.body()!!
        }
    }

}