package com.alejandromejia.recipesapp.data.database

import androidx.room.TypeConverter
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.Root
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class RecipesTypeConverter {

    var gson = Gson()

    @TypeConverter
    fun foodRecipeToString(foodRecipe: Root): String {
        return gson.toJson(foodRecipe)
    }

    @TypeConverter
    fun stringToFoodRecipe(data: String): Root {
        val listType = object : TypeToken<Root>() {}.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun resultToString(result: List<ExtendedIngredient>): String {
        return gson.toJson(result)
    }

    @TypeConverter
    fun stringToResult(data: String): List<ExtendedIngredient> {
        val listType = object : TypeToken<List<ExtendedIngredient>>() {}.type
        return gson.fromJson(data, listType)
    }

}