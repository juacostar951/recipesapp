package com.alejandromejia.recipesapp.data.database.dao

import androidx.room.*
import com.alejandromejia.recipesapp.data.database.entities.RootEntity

@Dao
interface RecipeDao {

    @Query("SELECT * FROM recipe_table")
    suspend fun getAllRecipesDB(): List<RootEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRecipe(root: RootEntity)

    @Query("SELECT * FROM recipe_table WHERE id = :id")
    suspend fun getById(id: Int): RootEntity

    @Delete
    suspend fun delete(root: RootEntity)
}