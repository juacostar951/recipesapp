package com.alejandromejia.recipesapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.alejandromejia.recipesapp.data.database.RecipesTypeConverter
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient

@Entity(tableName = "recipe_table")
data class RootEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "aggregateLikes") val aggregateLikes: Int,
    @TypeConverters(RecipesTypeConverter::class)
    val extendedIngredients: List<ExtendedIngredient>,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "readyInMinutes") val readyInMinutes: Int,
    @ColumnInfo(name = "servings") val servings: Int,
    @ColumnInfo(name = "summary") val summary: String,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "vegetarian") val vegetarian: Boolean,
)