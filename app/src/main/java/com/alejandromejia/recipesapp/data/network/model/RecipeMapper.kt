package com.alejandromejia.recipesapp.data.network.model

import com.alejandromejia.recipesapp.data.database.entities.RootEntity
import com.alejandromejia.recipesapp.data.model.Root

class RecipeMapper : EntityMapper<RootEntity, Root> {

    override fun mapFromEntity(entity: RootEntity): Root {
        return Root(
            id = entity.id,
            aggregateLikes = entity.aggregateLikes,
            image = entity.image,
            extendedIngredients = entity.extendedIngredients,
            readyInMinutes = entity.readyInMinutes,
            servings = entity.servings,
            summary = entity.summary,
            title = entity.title,
            vegetarian = entity.vegetarian
        )
    }

    override fun mapToEntity(model: Root): RootEntity {
        return RootEntity(
            id = model.id,
            aggregateLikes = model.aggregateLikes,
            image = model.image,
            extendedIngredients = model.extendedIngredients,
            readyInMinutes = model.readyInMinutes,
            servings = model.servings,
            summary = model.summary,
            title = model.title,
            vegetarian = model.vegetarian


        )
    }

    fun fromEntityList(initial: List<RootEntity>): List<Root> {
        return initial.map { mapFromEntity(it) }
    }

    fun toEntityList(initial: List<Root>): List<RootEntity> {
        return initial.map { mapToEntity(it) }
    }
}