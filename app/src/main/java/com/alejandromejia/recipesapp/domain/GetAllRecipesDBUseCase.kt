package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.Root
import javax.inject.Inject

class GetAllRecipesDBUseCase @Inject constructor(private val repository: RecipeRepository) {

    suspend operator fun invoke(): List<Root> = repository.getAllRecipesFromDB()

}