package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.FoodRecipe
import javax.inject.Inject


class GetRecipeUseCase @Inject constructor(private val repository: RecipeRepository) {

    suspend operator fun invoke(): FoodRecipe = repository.getAllRecipes()

}