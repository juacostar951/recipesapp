package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.FoodRecipeSimple
import javax.inject.Inject

class GetRecipeByFilterUseCase @Inject constructor(private val repository: RecipeRepository) {

    suspend operator fun invoke(query: String): FoodRecipeSimple =
        repository.getRecipesFilter(query)
}