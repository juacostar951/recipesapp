package com.alejandromejia.recipesapp.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alejandromejia.recipesapp.R
import com.alejandromejia.recipesapp.data.model.Recipe
import com.alejandromejia.recipesapp.ui.view.viewholder.RecipeViewHolder

class RecipeAdapter(
    private val recipe: List<Recipe>,
    private val onClickListener: (Recipe) -> Unit
) : RecyclerView.Adapter<RecipeViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return RecipeViewHolder(layoutInflater.inflate(R.layout.item_recipe, parent, false))
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        val item = recipe[position]
        holder.bind(item, onClickListener)
    }

    override fun getItemCount(): Int = recipe.size
}