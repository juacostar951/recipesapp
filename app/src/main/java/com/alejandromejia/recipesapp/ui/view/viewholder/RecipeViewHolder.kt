package com.alejandromejia.recipesapp.ui.view.viewholder

import android.annotation.SuppressLint
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.alejandromejia.recipesapp.data.model.Recipe
import com.alejandromejia.recipesapp.databinding.ItemRecipeBinding
import com.bumptech.glide.Glide

class RecipeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ItemRecipeBinding.bind(view)

    @SuppressLint("SetTextI18n")
    fun bind(recipe: Recipe, onClickListener: (Recipe) -> Unit) {

        val vegetarian: String

        if (recipe.vegetarian) {
            vegetarian = "Vegetarian"
        } else {
            vegetarian = "Non Vegetarian"
        }

        binding.tvTitle.text = recipe.title
        binding.tvLikes.text = "  ${recipe.aggregateLikes} likes"
        binding.tvServings.text = "  ${recipe.servings} Servings"
        binding.tvTime.text = "  Ready in ${recipe.readyInMinutes} minutes"
        binding.tvVegetarian.text = "  $vegetarian"
        Glide.with(binding.ivRecipe.context).load(recipe.image).into(binding.ivRecipe)
        binding.tvInformation.setOnClickListener { onClickListener(recipe) }
    }


}