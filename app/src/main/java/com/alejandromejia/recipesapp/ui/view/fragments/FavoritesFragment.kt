package com.alejandromejia.recipesapp.ui.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.text.HtmlCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.databinding.FragmentNotificationsBinding
import com.alejandromejia.recipesapp.ui.view.adapter.RecipeDBAdapter
import com.alejandromejia.recipesapp.ui.viewmodel.FavoritesViewModel
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoritesFragment : Fragment() {

    private var _binding: FragmentNotificationsBinding? = null
    private val binding get() = _binding!!
    private val favoritesViewModel by viewModels<FavoritesViewModel>()
    private var recipesList = mutableListOf<Root>()
    private var flag: Boolean = false
    lateinit var fullRecipe: Root
    private lateinit var mRecipeDBAdapter: RecipeDBAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.rvRecipes.isVisible = true
        binding.svSpecialFormMeasureInfo.isVisible = false
        flag = false

        favoritesViewModel.onCreate()
        initRecyclerViews()

        favoritesViewModel.completeRecipe.observe(this, Observer {

            initRecyclerViews()

            if (recipesList.size == 0) {
                val recipes = it
                recipesList.clear()
                recipesList.addAll(recipes)
            }

            binding.rvRecipes.isVisible = true
            binding.svSpecialFormMeasureInfo.isVisible = false


        })
        favoritesViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
        })

        favoritesViewModel.root.observe(this, Observer {


            if (flag) {
                binding.svSpecialFormMeasureInfo.isVisible = true
                completeRecipe(it)
            } else {
                binding.svSpecialFormMeasureInfo.isVisible = false
            }

        })

        binding.btnBack.setOnClickListener {
            onClickBack()
        }

        binding.btnFavorites.setOnClickListener {
            onClickFavorites()
        }

        initRecyclerViews()


        return root
    }

    private fun onClickFavorites() {

        favoritesViewModel.deleteFavorite(fullRecipe)
        recipesList.clear()
        initRecyclerViews()
        flag = false
        Toast.makeText(binding.rvRecipes.context, "The recipe has been removed from the favorites list", Toast.LENGTH_SHORT).show()

    }

    private fun onClickBack() {

        flag = false
        binding.rvRecipes.isVisible = true
        binding.svSpecialFormMeasureInfo.isVisible = false
    }

    private fun completeRecipe(completeRecipe: Root?) {

        fullRecipe = completeRecipe!!

        val vegetarian: String
        val result = completeRecipe?.summary
        val ingredients = mutableListOf<String>()

        if (completeRecipe?.vegetarian == true) {
            vegetarian = "Vegetarian"
        } else {
            vegetarian = "Non Vegetarian"
        }

        Glide.with(binding.ivRecipe.context).load(completeRecipe?.image).into(binding.ivRecipe)
        binding.tvTitle.text = completeRecipe?.title
        binding.tvLikes.text = "${completeRecipe?.aggregateLikes} likes"
        binding.tvServings.text = "${completeRecipe?.servings} Servings"
        binding.tvTime.text = "${completeRecipe?.readyInMinutes} minutes"
        binding.tvVegetarian.text = "$vegetarian"
        binding.tvSummary.text =
            result?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }

        for (i in completeRecipe?.extendedIngredients!!) {

            val name = i.name

            var firstLtr = name.substring(0, 1)
            val restLtrs = name.substring(1, name.length)

            firstLtr = firstLtr.uppercase()
            var ingredientName = firstLtr + restLtrs

            ingredients.add("*  $ingredientName -- ${i.original}")

        }

        val separator = "\n\n"

        val ingredientsStr = ingredients.joinToString(separator)

        binding.tvIngredients.text = ingredientsStr

    }

    private fun initRecyclerViews() {
        mRecipeDBAdapter = RecipeDBAdapter(recipesList) {
            onItemSelected(it)
        }

        binding.rvRecipes.layoutManager = LinearLayoutManager(binding.rvRecipes.context)
        binding.rvRecipes.adapter = mRecipeDBAdapter
    }

    private fun onItemSelected(recipe: Root) {

        flag = true

        binding.rvRecipes.isVisible = false
        favoritesViewModel.searchRecipeById(recipe.id)


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}