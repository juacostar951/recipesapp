package com.alejandromejia.recipesapp.ui.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.text.HtmlCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.alejandromejia.recipesapp.R
import com.alejandromejia.recipesapp.data.model.Recipe
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.databinding.FragmentHomeBinding
import com.alejandromejia.recipesapp.ui.view.adapter.RecipeAdapter
import com.alejandromejia.recipesapp.ui.viewmodel.HomeViewModel
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private lateinit var mRecipeAdapter: RecipeAdapter
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private var recipesList = mutableListOf<Recipe>()
    private var flag: Boolean = false
    lateinit var fullRecipe: Root
    private val homeViewModel by viewModels<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.rvRecipes.isVisible = true
        binding.svSpecialFormMeasureInfo.isVisible = false
        flag = false

        initRecyclerViews()
        if (recipesList.size == 0) {
            homeViewModel.onCreate()
        }


        homeViewModel.recipe.observe(this, Observer {

            if (recipesList.size == 0) {
                val recipes = it.recipes
                recipesList.clear()
                recipesList.addAll(recipes)
                mRecipeAdapter.notifyDataSetChanged()
            }

        })
        homeViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
        })

        homeViewModel.completeRecipe.observe(this, Observer {


            if (flag) {
                binding.svSpecialFormMeasureInfo.isVisible = true
                completeRecipe(it)
            } else {
                binding.svSpecialFormMeasureInfo.isVisible = false
            }

        })

        binding.btnBack.setOnClickListener {
            onClickBack()
        }

        binding.btnFavorites.setOnClickListener {
            onClickFavorites()
        }

        return root
    }

    private fun onClickFavorites() {

        binding.btnFavorites.setImageResource(R.drawable.ic_favorites)
        homeViewModel.saveFavorite(fullRecipe)
        Toast.makeText(binding.rvRecipes.context, "The recipe has been marked as a favorite", Toast.LENGTH_SHORT).show()

    }

    private fun onClickBack() {

        flag = false
        binding.rvRecipes.isVisible = true
        binding.svSpecialFormMeasureInfo.isVisible = false

    }

    @SuppressLint("SetTextI18n")
    private fun completeRecipe(completeRecipe: Root?) {
        binding.btnFavorites.setImageResource(R.drawable.ic_uncheck_favorites)
        fullRecipe = completeRecipe!!

        val vegetarian: String
        val result = completeRecipe?.summary
        val ingredients = mutableListOf<String>()

        if (completeRecipe?.vegetarian == true) {
            vegetarian = "Vegetarian"
        } else {
            vegetarian = "Non Vegetarian"
        }

        Glide.with(binding.ivRecipe.context).load(completeRecipe?.image).into(binding.ivRecipe)
        binding.tvTitle.text = completeRecipe?.title
        binding.tvLikes.text = "${completeRecipe?.aggregateLikes} likes"
        binding.tvServings.text = "${completeRecipe?.servings} Servings"
        binding.tvTime.text = "${completeRecipe?.readyInMinutes} minutes"
        binding.tvVegetarian.text = "$vegetarian"
        binding.tvSummary.text =
            result?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }

        for (i in completeRecipe?.extendedIngredients!!) {

            val name = i.name

            var firstLtr = name.substring(0, 1)
            val restLtrs = name.substring(1, name.length)

            firstLtr = firstLtr.uppercase()
            var ingredientName = firstLtr + restLtrs

            ingredients.add("*  $ingredientName -- ${i.original}")

        }

        val separator = "\n\n"

        val ingredientsStr = ingredients.joinToString(separator)

        binding.tvIngredients.text = ingredientsStr

    }

    private fun initRecyclerViews() {

        mRecipeAdapter = RecipeAdapter(recipesList) {
            onItemSelected(it)
        }

        binding.rvRecipes.layoutManager = LinearLayoutManager(binding.rvRecipes.context)
        binding.rvRecipes.adapter = mRecipeAdapter
    }

    private fun onItemSelected(recipe: Recipe) {

        flag = true

        binding.rvRecipes.isVisible = false
        homeViewModel.searchRecipeById(recipe.id)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}