package com.alejandromejia.recipesapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alejandromejia.recipesapp.data.model.FoodRecipe
import com.alejandromejia.recipesapp.data.model.FoodRecipeSimple
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.domain.GetRecipeByFilterUseCase
import com.alejandromejia.recipesapp.domain.GetRecipeById
import com.alejandromejia.recipesapp.domain.PutRecipeDbUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchRecipesViewModel @Inject constructor(
    private val getRecipeById: GetRecipeById,
    private val getRecipeByFilterUseCase: GetRecipeByFilterUseCase,
    private val putRecipeDbUseCase: PutRecipeDbUseCase
) : ViewModel() {

    val recipe = MutableLiveData<FoodRecipe>()
    val recipeSimple = MutableLiveData<FoodRecipeSimple>()
    val isLoading = MutableLiveData<Boolean>()
    val completeRecipe = MutableLiveData<Root>()


    fun getRecipesByFilter(query: String) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getRecipeByFilterUseCase(query)
            recipeSimple.postValue(result)
            isLoading.postValue(false)

        }
    }

    fun searchRecipeById(id: Int) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getRecipeById(id)

            if (result != null) {
                completeRecipe.postValue(result!!)
                isLoading.postValue(false)
            }

        }
    }

    fun saveFavorite(fullRecipe: Root) {
        viewModelScope.launch {
            isLoading.postValue(true)
            putRecipeDbUseCase(fullRecipe)
            isLoading.postValue(false)

        }

    }


}