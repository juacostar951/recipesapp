package com.alejandromejia.recipesapp

import android.app.Application
import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.room.Room
import com.alejandromejia.recipesapp.data.database.RecipeDataBase
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RecipeApp:Application(){

}